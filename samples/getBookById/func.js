const fdk = require('@fnproject/fdk');
const axios = require('axios');

const isbnRegex = /^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/;

fdk.handle(async function (input, ctx) {
  console.log('config', ctx.config)
  console.log('input', input)

  if (!input.isbn) {
    return { 'error': "No ISBN specified" }
  }
  if (!input.isbn.match(isbnRegex)) {
    return { 'error': "The given ISBN is not valid" }
  }

  let res = await axios.get(`http://host.docker.internal:3000/books?isbn=${input.isbn}`)
  let books = res.data;
  console.log('books', books)
  return {
    'message': 'Success',
    'book': books,
    'test': 'test'
  }
  
});
// echo -n '{"isbn": "9780596517748"}'|fn invoke bookApp getbookbyid