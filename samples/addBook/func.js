const fdk=require('@fnproject/fdk');
const axios = require('axios')

const isbnRegex = /^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/;

function Book(isbn, title, author, genre, pages, cover, summary) {
  return { isbn, title, author, genre, pages, cover, summary }
}

fdk.handle(async function (input, ctx) {

  let hctx = ctx.httpGateway
  hctx.setResponseHeader("Access-Control-Allow-Origin","*")
  hctx.setResponseHeader("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept")


  // check for given inputs
  if (!input.isbn) {
    return { 'error': "No ISBN specified" }
  }
  if (!input.title) {
    return { 'error': "No Title specified" }
  }  
  if (!input.author) {
    return { 'error': "No Author specified" }
  }  
  if (!input.genre) {
    return { 'error': "No Genre specified" }
  }  
  if (!input.pages) {
    return { 'error': "No Pages specified" }
  }  
  if (!input.cover) {
    return { 'error': "No Cover specified" }
  }  
  if (!input.summary) {
    return { 'error': "No Summary specified" }
  }

  //match isbn regex
  if (!input.isbn.match(isbnRegex)) {
    return { 'error': "The given ISBN is not valid" }
  }

  const bookToReturn = Book(input.isbn, input.title, input.author, input.genre, input.pages, input.cover, input.summary)
  console.log('book', {id:parseInt(input.isbn), ...bookToReturn, })
  let res = await axios.post(`http://host.docker.internal:3000/books/`, {id:parseInt(input.isbn), ...bookToReturn, } )
  let resultStatus = res.status;
  console.log('result', resultStatus)
  let message = (resultStatus == 201) ? "Success" : "Failed";
  return {
    'message': message,
  }

})
//echo -n '{"isbn":"9780596517799","title":"JavaScript: the super good parts","author":"Douglas Crockford","genre":"programming","pages":153,"cover":"https://someurl","summary":"Most programming languages ..."}'|fn invoke bookApp addbook