
```bash
fn create app bookApp
cd ../addBook
fn deploy --app bookApp --local
cd ../addBookByISBN
fn deploy --app bookApp --local
cd ../deleteBook
fn deploy --app bookApp --local
cd ../getAllBooks
fn deploy --app bookApp --local
cd ../getBookById
fn deploy --app bookApp --local
cd ../getCoverForBook
fn deploy --app bookApp --local
fn list functions bookApp

# output
NAME            IMAGE                   ID
addbook		    addbook:0.0.15		    01DRTZ9DCGNG8G00GZJ000048F
addbookbyisbn	addbookbyisbn:0.0.20	01DRTZ9FC0NG8G00GZJ000048G
deletebook	    deletebook:0.0.12	    01DRTZ9GVNNG8G00GZJ000048H
getallbooks	    getallbooks:0.0.6	    01DRTZ9JFTNG8G00GZJ000048J
getbookbyid	    getbookbyid:0.0.59	    01DRTZ9SSGNG8G00GZJ000048K
getcoverforbook	getcoverforbook:0.0.10	01DRVG0T0FNG8G00GZJ000001J
```
Now our functions are deployed but can not be accessed from the webapp. Therefore have to define triggers
```bash
#create trigger for function
fn create trigger bookApp getbookbyid getbookbyidtrigger --type http --source /getbookbyid
fn create trigger bookApp getallbooks getallbookstrigger --type http --source /getallbooks
fn create trigger bookApp addbook addbooktrigger --type http --source /addbook
fn create trigger bookApp addbookbyisbn addbookbyisbntrigger --type http --source /addbookbyisbn
fn create trigger bookApp deletebook deletebooktrigger --type http --source /deletebook
fn create trigger bookApp getcoverforbook getcoverforbooktrigger --type http --source /getcoverforbook

fn list triggers bookApp
# output:
FUNCTION        NAME		            ID                          TYPE    SOURCE              ENDPOINT
addbookbyisbn	addbookbyisbntrigger	01DRTZJ00BNG8G00GZJ000048R	http	/addbookbyisbn	    http://localhost:8080/t/bookApp/addbookbyisbn
addbook		    addbooktrigger		    01DRTZFK6DNG8G00GZJ000048P	http	/addbook		    http://localhost:8080/t/bookApp/addbook
deletebook	    deletebooktrigger	    01DRTZFK96NG8G00GZJ000048Q	http	/deletebook		    http://localhost:8080/t/bookApp/deletebook
getallbooks	    getallbookstrigger	    01DRTZFK4PNG8G00GZJ000048N	http	/getallbooks		http://localhost:8080/t/bookApp/getallbooks
getbookbyid	    getbookbyidtrigger	    01DRTZFK2SNG8G00GZJ000048M	http	/getbookbyid		http://localhost:8080/t/bookApp/getbookbyid
getcoverforbook	getcoverforbooktrigger	01DRVG3QHSNG8G00GZJ000001K	http	/getcoverforbook	http://localhost:8080/t/bookApp/getcoverforbook
```
Try the webapp again. Now there should be books available