const fdk = require('@fnproject/fdk');
const axios = require('axios');

fdk.handle(async function (input, ctx) {
  console.log('config', ctx.config)
  console.log('input', input)

  let hctx = ctx.httpGateway
  hctx.setResponseHeader("Access-Control-Allow-Origin","*")
  hctx.setResponseHeader("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept")

  let res = await axios.get(`http://host.docker.internal:3000/books`)
  let books = res.data;
  console.log('books', books)
  return {
    'message': 'Success',
    'books': books,
    'test': 'test'
  }
  
});