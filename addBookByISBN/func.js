const fdk = require('@fnproject/fdk');
const axios = require('axios')
const _ = require('lodash')

const isbnRegex = /^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/;

function Book(isbn, title, author, genre, pages, cover, summary) {
  return { isbn, title, author, genre, pages, cover, summary }
}

fdk.handle(async function (input, ctx) {

  let hctx = ctx.httpGateway
  hctx.setResponseHeader("Access-Control-Allow-Origin", "*")
  hctx.setResponseHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")


  // check for given inputs
  if (!input.isbn) {
    return { 'error': "No ISBN specified" }
  }

  //match isbn regex
  if (!input.isbn.match(isbnRegex)) {
    return { 'error': "The given ISBN is not valid" }
  }

  //try to get book from openlibrary
  // 'validateStatus': () => {return true}
  let resSpecificBook = await axios.get(`http://openlibrary.org/isbn/${input.isbn}.json`, { 'headers': { 'Accept': 'application/json' }, 'validateStatus': () => { return true } })
  
  let olBook = resSpecificBook.data;

  let authorkey = _.get(olBook, 'authors[0].key', 0)
  let olAuthor = ''
  if (authorkey !== 0) {
    let resSpecificAuthor = await axios.get(`http://openlibrary.org/api/get?key=${olBook.authors[0].key}`)
    olAuthor = _.get(resSpecificAuthor, 'data.result.name', '');
  }
  const bookToReturn = Book(input.isbn, olBook.title, olAuthor, _.get(olBook, 'subjects[0]', ''), _.get(olBook, 'number_of_pages', ''), _.get(olBook, 'covers[0]', ''), _.get(olBook, 'description.value', ''))

  let resGoogleBook = await axios.get(`https://www.googleapis.com/books/v1/volumes?q=isbn:${input.isbn}`, { 'headers': { 'Accept': 'application/json' }, 'validateStatus': () => { return true } })
  let gBook = resGoogleBook.data.items[0];
  const googleBookToReturn = Book(input.isbn, _.get(gBook, 'volumeInfo.title', ''), _.get(gBook, 'volumeInfo.authors[0]', ''), _.get(gBook, 'volumeInfo.categories[0]', ''), _.get(gBook, 'volumeInfo.pageCount', ''), _.get(gBook, 'volumeInfo.imageLinks.thumbnail', ''), _.get(gBook, 'volumeInfo.description', ''))

  const combinedBookTitle = bookToReturn.title ? bookToReturn.title : googleBookToReturn.title
  const combinedBookAuthor = bookToReturn.author ? bookToReturn.author : googleBookToReturn.author
  const combinedBookGenre = bookToReturn.genre ? bookToReturn.genre : googleBookToReturn.genre
  const combinedBookPages = bookToReturn.pages ? bookToReturn.pages : googleBookToReturn.pages
  const combinedBookCoverUrl = bookToReturn.cover ? bookToReturn.cover : googleBookToReturn.cover
  const combinedBookSummary = bookToReturn.summary ? bookToReturn.summary : googleBookToReturn.summary
  const combinedBook = Book(input.isbn, combinedBookTitle, combinedBookAuthor, combinedBookGenre, combinedBookPages, combinedBookCoverUrl, combinedBookSummary)

  console.log('_____bookToRetrun______')
  console.log(bookToReturn)
  console.log('_____googleBookToReturn______')
  console.log(googleBookToReturn)

  if ((resSpecificBook.status < 200 || resSpecificBook.status >= 300) && (googleBookToReturn.status < 200 || googleBookToReturn.status >=300)){
    return {
      'message': 'Failed'
    }
  }
  return {
    'message': 'Success',
    "book": combinedBook
  }
})