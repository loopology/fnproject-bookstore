To be able to create books from the frontend, you should create the function addBook(). 
It takes several book attributes as input and makes a database call to store to book persistent in the database. 
Afterwards, there should be an output, if the operation was successfull or not.

Input:  HTTP Post @ http://localhost:8080/t/bookApp/addbook
        Body type JSON: 
     { 
          "isbn":"<some-isbn>",
          "title":<some-title>,
          "author":"<some-author",
          "genre":"<some-genre>",
          "pages":<some-pagenumber>,
          "cover":"<some-cover-id>",
          "summary":"<some-summary"
     }
Check if given inputs are valid (at least if all inputs are given)

        
DB connection: HTTP Post @ http://host.docker.internal:3000/books/
Body type JSON: {
     "id": <some-isbn-here>
     "isbn":"<some-isbn>",
     "title":<some-title>,
     "author":"<some-author",
     "genre":"<some-genre>",
     "pages":<some-pagenumber>,
     "cover":"<some-cover-id>",
     "summary":"<some-summary"
}

Output Message:
     {'message': 'Success||failed'}