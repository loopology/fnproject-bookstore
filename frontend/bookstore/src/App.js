import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import BookstoreNavbar from './BookstoreNavbar/BookstoreNavbar';
import Booklist from './Booklist/Booklist';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import CreateNewBook from './CreateNewBook/CreateNewBook';
import axios from 'axios';


class App extends Component {
  constructor(props) {
    super(props)
    this.child = React.createRef();
  }

  componentDidMount() {
    axios.interceptors.request.use(config => {
      this.setState({ spinner: true })
      return config
    }, error => {
      this.setState({ spinner: false })
      return Promise.reject(error)
    });

    axios.interceptors.response.use(response => {
      this.setState({ spinner: false })
      return response;
    }, error => {
      this.setState({ spinner: false })
      return Promise.reject(error)
    });
  }


  state = {
    spinner: false
  }

  reloadBooks = () => {
    this.child.current.getAllBooks()
  }
  render() {
    return (
      <div className="App">
        {this.state.spinner && <div className="fullscreenspinner"><div className="lds-dual-ring"></div></div>}
        <BookstoreNavbar />
        <Container fluid={true} >
          <Row >
            <Col lg={8}>
              <Booklist ref={this.child} />
            </Col>
            <Col lg={4}>
              <CreateNewBook reloadBooks={() => this.reloadBooks()} />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
