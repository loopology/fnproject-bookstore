import React from 'react';
import Card from 'react-bootstrap/Card'
import ListGroup from 'react-bootstrap/ListGroup'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import './BookDetail.css'
import axios from 'axios'
import API_URI from '../constants';

class BookDetail extends React.Component {

  state = {
    cover: false
  }
  componentDidMount() {
    if(this.props.book.isbn) {
      axios.post(API_URI + 'getcoverforbook', {isbn: this.props.book.isbn}).then(result => {
        console.log('functioncalled')
        console.log('someresult',result)
        const image = 'data:image/jpeg;base64,'.concat(result.data.image)      
        this.setState({cover: image})
      })
    }
  }

  render() {
    return (
      <Modal show={this.props.book && true} onHide={() => this.props.setOpen(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Book Details</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Card>
          {this.state.cover && <Card.Img className={'bookcover'} variant="top" src={this.state.cover} />}
            <Card.Body>
              <Card.Title>{this.props.book.title}</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">by {this.props.book.author}</Card.Subtitle>
              <ListGroup variant="flush">
                <ListGroup.Item><b>Genre: </b>{this.props.book.genre}</ListGroup.Item>
                <ListGroup.Item><b>Pages: </b>{this.props.book.pages}</ListGroup.Item>
                <ListGroup.Item><b>ISBN: </b>{this.props.book.isbn}</ListGroup.Item>
              </ListGroup>
              <Card.Text>
                {this.props.book.summary}
              </Card.Text>
            </Card.Body>
          </Card>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => this.props.setOpen(false)}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default BookDetail;
