import React from 'react';
import Navbar from 'react-bootstrap/Navbar'

function BookstoreNavbar() {
  return (
        <Navbar bg="light">
          <Navbar.Brand href="#home">Bookstore</Navbar.Brand>
        </Navbar>
  );
}

export default BookstoreNavbar;
