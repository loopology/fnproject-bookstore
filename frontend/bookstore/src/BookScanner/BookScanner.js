import React from 'react';
import './BookScanner.css'
import Quagga from 'quagga'
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
class BookScanner extends React.Component {

  state = {
    isbn: false,
  }
  componentDidMount() {
    Quagga.init({
      inputStream: {
        name: "Live",
        type: "LiveStream",
        target: document.querySelector('#quaggavideo'),
      },
      decoder: {
        readers: [
          "ean_reader"
        ],
        halfSample: false,
        patchSize: "medium",
        debug: {
          showCanvas: true,
          showPatches: true,
          showFoundPatches: true,
          showSkeleton: true,
          showLabels: true,
          showPatchLabels: true,
          showRemainingPatchLabels: true,
          boxFromPatches: {
            showTransformed: true,
            showTransformedBox: true,
            showBB: true
          }
        }
      }
    }, function (err) {
      if (err) {
        console.log(err);
        return
      }
      console.log("Initialization finished. Ready to start");
      Quagga.start();

    });

    Quagga.onDetected(result => {
      console.log('Quagga result', result)
      this.props.setOpen({open: false, isbn: result.codeResult.code})
    })
    // this.setState({ isbn: "1234567899876" })
    // this.props.setOpen({open: false, isbn: "1234567899876"})
  }
  componentWillUnmount() {
    Quagga.stop()
  }

  render() {
    // {open: false, isbn: ""}
    return (
      <Modal show={this.props.isOpen} onHide={() => this.props.setOpen({open: false, isbn: ""})}>
        <Modal.Header closeButton>
          <Modal.Title>ISBN Scanner</Modal.Title>
        </Modal.Header>
        <Modal.Body>

        {this.state.isbn}
          <div id="quaggavideo">
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => this.props.setOpen({open: false, isbn: ""})}>
            Close
        </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default BookScanner;
