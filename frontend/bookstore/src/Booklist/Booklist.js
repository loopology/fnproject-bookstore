import React from 'react';
import Card from 'react-bootstrap/Card'
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Delete from '@material-ui/icons/Delete'
import RefreshIcon from '@material-ui/icons/Refresh';
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import './Booklist.css'
import BookDetail from '../BookDetail/BookDetail';
import axios from 'axios'
import API_URI from '../constants';

function createData(isbn, title, author, genre, pages, cover, summary) {
  return { isbn, title, author, genre, pages, cover, summary };
}

makeStyles({
  table: {
    minWidth: 650,
  }
});

class Booklist extends React.Component {

  state = {
    isbn: false,
    bookDetailOpen: false,
    rows: []
  }

  getAllBooks = () => {
    axios.get(API_URI + 'getallbooks').then(result => {
      this.state.rows.length !== 0 && this.setState({ rows: [] })
      result.data.books.forEach(item => {
        const newRow = createData(item.isbn, item.title, item.author, item.genre, item.pages, item.cover, item.summary)
        this.setState(prevState => ({
          rows: [...prevState.rows, newRow]
        }))
      })
    }
    )
  }

  deleteBook = (isbn) => {
    axios.delete(API_URI + 'deletebook', { data: { isbn: isbn } }).then(result => {
      console.log('delete')
      this.getAllBooks()
    })
  }

  componentDidMount() {
    this.getAllBooks()
  }

  render() {
    return (
      <Card className="text-center">
        <Card.Header>Booklist<span className={'refresh'} onClick={this.getAllBooks}><RefreshIcon /></span></Card.Header>
        <Card.Body>
          <Table aria-label="simple table" >
            <TableHead>
              <TableRow>
                <TableCell>ISBN</TableCell>
                <TableCell align="left">Title</TableCell>
                <TableCell align="right">Author</TableCell>
                <TableCell align="right">Genre</TableCell>
                <TableCell align="right">Pages</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.rows.map(row => (
                <TableRow hover key={row.isbn}>
                  <TableCell onClick={() => this.setState({ bookDetailOpen: row })} component="th" scope="row"><b>{row.isbn}</b></TableCell>
                  <TableCell onClick={() => this.setState({ bookDetailOpen: row })} align="left">{row.title}</TableCell>
                  <TableCell onClick={() => this.setState({ bookDetailOpen: row })} align="right">{row.author}</TableCell>
                  <TableCell onClick={() => this.setState({ bookDetailOpen: row })} align="right">{row.genre}</TableCell>
                  <TableCell onClick={() => this.setState({ bookDetailOpen: row })} align="right">{row.pages}</TableCell>
                  <TableCell align="right"><Delete onClick={() => this.setState({ isbn: row.isbn })} /></TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Card.Body>
        <DeleteModal isbn={this.state.isbn} setOpen={value => this.setState({ isbn: value })} delete={this.deleteBook} />
        {this.state.bookDetailOpen && <BookDetail book={this.state.bookDetailOpen} setOpen={value => this.setState({ bookDetailOpen: value })} />}
      </Card>
    );
  }
}

function DeleteModal(props) {
  const sendDelete = () => {
    console.log('props', props)
    props.setOpen(false)
    props.delete(props.isbn);
  }
  return (
    <>
      <Modal show={props.isbn && true} onHide={() => props.setOpen(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Book?</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure you want to delete the Book with the ISBN {props.isbn}?</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => props.setOpen(false)}>
            No!
        </Button>
          <Button variant="primary" onClick={() => sendDelete()}>
            Yes!
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Booklist;
